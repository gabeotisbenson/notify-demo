import Vue from 'vue';
import DemoForm from '~/vue/DemoForm.vue';

const app = new Vue({
	el: '#demo-form',
	render: h => h(DemoForm)
});

export default app;
